import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:emotion/database/app_preferences.dart';
import 'package:emotion/services/index.dart';

class SplashViewModel with ChangeNotifier {
  BuildContext context;

  void attachContext(BuildContext context) {
    this.context = context;
  }
}
