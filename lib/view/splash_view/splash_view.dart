import 'dart:async';
import 'dart:ui';

import 'package:emotion/generated/i18n.dart';
import 'package:emotion/utils/common_colors.dart';
import 'package:emotion/utils/text_style.dart';
import 'package:emotion/view/splash_view/splash_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  var scaffoldKey = new GlobalKey<ScaffoldState>();

  //FirebaseMessaging _firebaseMessaging;
  SplashViewModel mViewModel;

  @override
  void initState() {
    super.initState();
    /*_firebaseMessaging = FirebaseMessaging.instance;
    _firebaseMessaging.getToken().then((String token) {
      printf("firebase token=>" + token.toString());
    });*/
    //AppPreferences().setLoginMaster("");
    new Future.delayed(const Duration(seconds: 3), () {
      mViewModel = Provider.of<SplashViewModel>(context, listen: false);
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SplashViewModel>(context);
    print(String.fromCharCode(248));
    print("Get started");

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: CommonColors.primaryColor,
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              S.of(context).appName,
              textAlign: TextAlign.center,
              style: CommonStyle.getAppStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: CommonColors.whiteColor),
            )
          ],
        ),
      ),
    );
  }
}
