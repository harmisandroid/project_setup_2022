import 'dart:async';
import 'dart:convert';
import 'package:emotion/generated/i18n.dart';
import 'package:emotion/models/app.dart';
import 'package:emotion/utils/common_colors.dart';
import 'package:emotion/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'database/app_preferences.dart';
import 'services/index.dart';
import 'view/splash_view/splash_view.dart';
import 'package:provider/provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'view/splash_view/splash_view_model.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

final GlobalKey<NavigatorState> mainNavigatorKey = GlobalKey<NavigatorState>();
final routeObserver = RouteObserver<PageRoute>();

class AppState extends State<App> /*with WidgetsBindingObserver*/ {
  static BuildContext appContext;
  final _app = AppModel();
  AppPreferences appPreferences = new AppPreferences();
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  var id;
  StreamSubscription<ConnectivityResult> subscription;
  final Connectivity _connectivity = Connectivity();
  var isLaunch = true;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {}
  }

  @override
  void initState() {
    super.initState();
    Services().setAppConfig();
    subscription = _connectivity.onConnectivityChanged
        .listen((ConnectivityResult result) async {
      // Got a new connectivity status!
      result = await _connectivity.checkConnectivity();
      print("network changed: " + result.toString());

      if (!isLaunch) {
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
          print("back");
          Navigator.pop(mainNavigatorKey.currentContext);
        } else {
          print("push");
        }
      } else {
        print("is firsttime false");
        isLaunch = false;
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
        } else {
          print("push");
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    appContext = context;
    return ChangeNotifierProvider<AppModel>.value(
      value: _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          value.isLoading = false;
          if (value.isLoading) {
            return Container(
              color: Theme.of(context).backgroundColor,
            );
          }
          return MultiProvider(
            providers: [
              ChangeNotifierProvider<SplashViewModel>(
                  create: (_) => SplashViewModel()),
            ],
            child: MaterialApp(
              theme: ThemeData(
                  appBarTheme: Theme.of(context)
                      .appBarTheme
                      .copyWith(brightness: Brightness.light),
                  primarySwatch: Colors.blue,
                  visualDensity: VisualDensity.adaptivePlatformDensity),
              builder: (BuildContext context, Widget child) {
                final MediaQueryData data = MediaQuery.of(context);
                return MediaQuery(
                    child: child, data: data.copyWith(textScaleFactor: 1));
              },
              title: 'Park App',
              navigatorKey: mainNavigatorKey,
              navigatorObservers: [routeObserver],
              debugShowCheckedModeBanner: false,
              initialRoute: '/',
              locale: new Locale(Provider.of<AppModel>(context).locale, ""),
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              localeListResolutionCallback: S.delegate.listResolution(
                  fallback: const Locale(AppConstants.LANGUAGE_GERMAN, '')),
              home: SplashView(),
              routes: <String, WidgetBuilder>{},
            ),
          );
        },
      ),
    );
  }
}
