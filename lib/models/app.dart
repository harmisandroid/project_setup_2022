import 'dart:convert';

import 'package:emotion/database/app_preferences.dart';
import 'package:emotion/utils/common_utils.dart';
import 'package:emotion/utils/constant.dart';
import 'package:flutter/material.dart';

class AppModel with ChangeNotifier {
  bool darkTheme = false;
  bool isLoading = true;
  String locale = AppConstants.LANGUAGE_GERMAN;
  AppPreferences appPreferences = new AppPreferences();

  void changeLanguage() async {
    String locale = await appPreferences.getLanguageCode();
    if (CommonUtils.isEmpty(locale)) {
      appPreferences.setLanguageCode(this.locale);
      locale = this.locale;
    }
    this.locale = locale;
    notifyListeners();
  }
}
