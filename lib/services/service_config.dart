import 'base_services.dart';

mixin ConfigMixin {
  BaseServices api;

  void configBase({BaseServices apiServices}) {
    setAppConfig();
    api = apiServices;
  }

  void configAPI() {}

  void setAppConfig() {
    configAPI();
  }
}
