import 'service_config.dart';
import 'api_mixin.dart';

class Services with ConfigMixin, ApiMixin {
  static final Services _instance = Services._internal();

  factory Services() => _instance;

  Services._internal();
}