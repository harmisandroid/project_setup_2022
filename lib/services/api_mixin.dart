import 'package:emotion/services/api_service.dart';
import 'package:emotion/services/base_services.dart';

import 'service_config.dart';

mixin ApiMixin on ConfigMixin {
  @override
  void configAPI() {
    api = ApiServices();
  }
}