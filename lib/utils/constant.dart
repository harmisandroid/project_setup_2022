

class AppConstants {
  /*--------Language Codes-------------------*/
  static const String LANGUAGE_ENGLISH = 'en';
  static const String LANGUAGE_GERMAN = 'de';
}
