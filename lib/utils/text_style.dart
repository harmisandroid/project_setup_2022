import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';

extension CapExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.split(" ").map((str) => str[0].toUpperCase() + str.substring(1)).join(" ");
}

class CommonStyle {
  static TextStyle getAppStyle(
      {Color color,
      double fontSize,
      FontWeight fontWeight,
      FontStyle fontStyle,
      TextDecoration decoration,
      double height,
      double letterSpacing}) {
    return TextStyle(
      fontFamily: "Lexend",
      color: color,
      letterSpacing: letterSpacing == null ? 0.3 : letterSpacing,
      decoration: decoration != null ? decoration : TextDecoration.none,
      fontSize: fontSize,
      fontWeight: fontWeight,
      fontStyle: fontStyle == null ? FontStyle.normal : fontStyle,
      height: height,
    );
  }

  static TextStyle getRobotoFont(
      {Color color,
        double fontSize,
        FontWeight fontWeight,
        FontStyle fontStyle,
        TextDecoration decoration,
        double height,
        double letterSpacing}) {
    return GoogleFonts.roboto(
      textStyle: TextStyle(
        color: color,
        letterSpacing: letterSpacing == null ? 0.3 : letterSpacing,
        decoration: decoration != null ? decoration : TextDecoration.none,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle == null ? FontStyle.normal : fontStyle,
        height: height,
      )
    );
  }
}
